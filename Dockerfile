FROM openjdk:8
ADD target/voiceofindia.jar voiceofindia.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "voiceofindia.jar"]