package com.css.voi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.css.voi.model.PostInfo;

public interface PostRepository extends JpaRepository<PostInfo,Long>{

}
