package com.css.voi.repository;

import org.springframework.data.repository.CrudRepository;

import com.css.voi.model.Comment;

public interface CommentRepository extends CrudRepository<Comment,Integer>{

}
