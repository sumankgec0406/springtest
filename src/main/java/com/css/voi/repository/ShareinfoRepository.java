package com.css.voi.repository;

import org.springframework.data.repository.CrudRepository;

import com.css.voi.model.Shareinfo;

public interface ShareinfoRepository extends CrudRepository<Shareinfo,Integer> {

}
