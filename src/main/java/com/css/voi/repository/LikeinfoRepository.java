package com.css.voi.repository;

import org.springframework.data.repository.CrudRepository;

import com.css.voi.model.Likeinfo;

public interface LikeinfoRepository extends CrudRepository<Likeinfo,Integer> {

}
