package com.css.voi.repository;

import org.springframework.data.repository.CrudRepository;

import com.css.voi.model.Commentlikeinfo;

public interface CommentlikeinfoRepository extends CrudRepository<Commentlikeinfo,Integer> {

}
