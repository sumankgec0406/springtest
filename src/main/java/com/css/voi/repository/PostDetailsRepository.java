package com.css.voi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.css.voi.model.Post_details;

public interface PostDetailsRepository extends CrudRepository<Post_details,Long> {
	
}
