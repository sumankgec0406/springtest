package com.css.voi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.css.voi.model.Likeinfo;

import com.css.voi.service.LikeinfoService;


@RestController
@RequestMapping("/voiceofindia-posts")
public class LikeinfoController {
	

	@Autowired
	private LikeinfoService likeinfoService;
	
	@RequestMapping("/likeinfo")
	public List<Likeinfo> getAllLikeinfo() {
	   return likeinfoService.getAllLikeinfo();
	}
   
	@RequestMapping("/likeinfo/{id}")
	public Optional<Likeinfo> getShareinfo(@PathVariable int id) {
		return likeinfoService.getLikeinfo(id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/likeinfo")
	public void addShareinfo(@RequestBody Likeinfo likeinfo) {
		likeinfoService.addLikeinfo(likeinfo);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/likeinfo/{id}")
	public void updateShareinfo(@RequestBody Likeinfo likeinfo, @PathVariable int id ) {
		likeinfoService.updateLikeinfo(likeinfo,id);
		
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/likeinfo/{id}")
	public void deleteLikeinfo(@PathVariable int id) {
		likeinfoService.deleteLikeinfo(id);
		
	}

}
