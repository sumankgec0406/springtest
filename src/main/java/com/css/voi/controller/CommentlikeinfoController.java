package com.css.voi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.css.voi.model.Commentlikeinfo;
import com.css.voi.service.CommentlikeinfoService;


@RestController
@RequestMapping("/voiceofindia-posts")
public class CommentlikeinfoController {
	
	@Autowired
	private CommentlikeinfoService commentlikeinfoService;
	
	@RequestMapping("/commentlikeinfo")
	public List<Commentlikeinfo> getAllCommentlikeinfo() {
	   return commentlikeinfoService.getAllCommentlikeinfo();
	}
   
	@RequestMapping("/commentlikeinfo/{id}")
	public Optional<Commentlikeinfo> getCommentlikeinfo(@PathVariable int id) {
		return commentlikeinfoService.getCommentlikeinfo(id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/commentlikeinfo")
	public Commentlikeinfo addCommentlikeinfo(@RequestBody Commentlikeinfo commentlikeinfo) {
		return commentlikeinfoService.addCommentlikeinfo(commentlikeinfo);
		
	}
	@RequestMapping(method=RequestMethod.PUT, value="/commentlikeinfo/{id}")
	public Commentlikeinfo updateCommentlikeinfo(@RequestBody Commentlikeinfo commentlikeinfo, @PathVariable int id ) {
		return commentlikeinfoService.updateCommentlikeinfo(commentlikeinfo,id);
		
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/commentlikeinfo/{id}")
	public void deleteCommentlikeinfo(@PathVariable int id) {
		commentlikeinfoService.deleteCommentlikeinfo(id);
		
	}

}
