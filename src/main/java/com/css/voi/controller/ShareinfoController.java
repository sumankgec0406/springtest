package com.css.voi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.css.voi.model.Shareinfo;
import com.css.voi.service.ShareinfoService;



@RestController
@RequestMapping("/voiceofindia-posts")
public class ShareinfoController {
	
	@Autowired
	private ShareinfoService shareinfoService;
	
	@RequestMapping("/shareinfo")
	public List<Shareinfo> getAllShareinfo() {
	   return shareinfoService.getAllShareinfo();
	}
   
	@RequestMapping("/shareinfo/{id}")
	public Optional<Shareinfo> getShareinfo(@PathVariable int id) {
		return shareinfoService.getShareinfo(id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/shareinfo")
	public Shareinfo addShareinfo(@RequestBody Shareinfo shareinfo) {
		return shareinfoService.addShareinfo(shareinfo);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/shareinfo/{id}")
	public Shareinfo updateShareinfo(@RequestBody Shareinfo shareinfo, @PathVariable int id ) {
		return shareinfoService.updateShareinfo(shareinfo,id);
		
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/shareinfo/{id}")
	public void deleteShareinfo(@PathVariable int id) {
		shareinfoService.deleteShareinfo(id);
		
	}
}
