package com.css.voi.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.css.voi.model.PostInfo;
import com.css.voi.service.PostService;

@RestController
@RequestMapping("/voiceofindia-posts")
public class PostController {
	@Autowired
	PostService postService;
	
	
	@PostMapping("/post")
	public ResponseEntity<Object> createPost(@RequestBody PostInfo post) {
		PostInfo postinfo=postService.save(post);
		
		URI location=ServletUriComponentsBuilder.
				fromCurrentRequest().
				path("/{post_id}").
				buildAndExpand(postinfo.getPost_id()).toUri();
		return ResponseEntity.created(location).build();
		
	}
	
	
	@GetMapping("/allpost")
	public List<PostInfo> getAllPost(){
		return postService.findAllPost();
		
	}
	
	
	@GetMapping("/postbyid/{post_id}")
	public ResponseEntity<Optional<PostInfo>> getPostInfoById(@PathVariable(value="post_id") Long post_id) {
		Optional<PostInfo> posts=postService.findPostById(post_id);
		
		return ResponseEntity.ok().body(posts);
	}
	
	@PutMapping("/updatepostinfo/{post_id}")
	public void updatePostinfo(@RequestBody PostInfo post, @PathVariable Long post_id) {
		postService.updatePostInfo(post, post_id);
		
	}
	
	@DeleteMapping("/deletepostinfo/{post_id}")
	public void deletepostinfo(@PathVariable Long post_id) {
		postService.deletePostInfo(post_id);
		
	}
}
