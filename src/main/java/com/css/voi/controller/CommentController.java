package com.css.voi.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.css.voi.model.Comment;
import com.css.voi.service.CommentService;

@RestController
@RequestMapping("/voiceofindia-posts")
public class CommentController {
	
	@Autowired
	CommentService commentService;
	
	/* to post comment details */
	@PostMapping("/comments")
	public ResponseEntity<Object> saveCommentdetails(@RequestBody Comment comment){
		Comment comments=commentService.saveComment(comment);
		
		URI location=ServletUriComponentsBuilder.
				fromCurrentRequest().
				path("/{commentid}").
				buildAndExpand(comments.getCommentid()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	/* to get all comment details */
	@GetMapping("/allcomments")
	public List<Comment> getallcomments(){
		return commentService.getCommentdetails();
	}
	
	/* to get comment by id */
	@GetMapping("/commentbyid/{commentid}")
	public Optional<Comment> getCommentsbyid(@PathVariable int commentid) {
		Optional<Comment> comments=commentService.getCommentBiId(commentid);
		return comments;
	}
	
	@PutMapping("/updatecomment/{commentid}")
	public Comment updateComment(@RequestBody Comment comment,@PathVariable int commentid) {
		return commentService.updatecomment(comment, commentid);
	}
	
	@DeleteMapping("/deletecomment/{commentid}")
	public void deleteComment(@PathVariable int commentid) {
		commentService.deletecomment(commentid);
	}
}
