package com.css.voi.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.css.voi.model.Post_details;
import com.css.voi.service.PostDetailsService;

@Controller
@RequestMapping("/voiceofindia-posts")
public class PostDetailsController {
	@Autowired
	PostDetailsService postDetailsService;
	
	/* to post post_details */
	@PostMapping("/post-details")
	public ResponseEntity<Object> createPostDetails(@RequestBody Post_details posts) {
		Post_details postdetails=postDetailsService.save(posts);
		
		URI location=ServletUriComponentsBuilder.
				fromCurrentRequest().
				path("/{post_detailsId}").
				buildAndExpand(postdetails.getPost_detailsId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	/* to get all post details */
	@RequestMapping("/allpost-details")
	public List<Post_details> getAllPostDetails(){
		return postDetailsService.findAllPostDetails();
	}
	
	/* to get post details by id */
	@GetMapping("/getpostdetailsbyid/{post_detailsId}")
	public Optional<Post_details> getPostdetailsById(@PathVariable Long post_detailsId) {
		Optional<Post_details> posts=postDetailsService.findPostDetailsById(post_detailsId);
		return posts;
	}
	
	/* to update post_details*/
	@PutMapping("/updatepostdetails/{post_detailsId}")
	public void updatePostdetails(@RequestBody Post_details posts, @PathVariable Long post_detailsId) {
		postDetailsService.updatePostDetails(posts, post_detailsId);
	}
	
	/* to delete post_details */
	@DeleteMapping("/deletepostdetails/{post_detailsId}")
	public void deletePostDetails(@PathVariable Long post_detailsId) {
		postDetailsService.deletePost(post_detailsId);
	}
}
