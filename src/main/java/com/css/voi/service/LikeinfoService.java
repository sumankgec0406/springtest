package com.css.voi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.css.voi.model.Likeinfo;
import com.css.voi.repository.LikeinfoRepository;

@Service
public class LikeinfoService {

@Autowired
private LikeinfoRepository likeinfoRepository;

	

	public void addLikeinfo(Likeinfo likeinfo) {
		
		// TODO Auto-generated method stub
		likeinfoRepository.save(likeinfo);
	}

	public void updateLikeinfo(Likeinfo likeinfo, int id) {
		// TODO Auto-generated method stub	
		likeinfoRepository.save(likeinfo);
	}

	public void deleteLikeinfo(int id) {
		// TODO Auto-generated method stub
		likeinfoRepository.deleteById(id);
		
	}



	public Optional<Likeinfo> getLikeinfo(int id) {
		// TODO Auto-generated method stub
		return likeinfoRepository.findById(id);
		
	}

	public List<Likeinfo> getAllLikeinfo() {
		// TODO Auto-generated method stub
		List<Likeinfo> likes=new ArrayList<Likeinfo>();
		likeinfoRepository.findAll().forEach(likes::add);
		return likes;
	}

}
