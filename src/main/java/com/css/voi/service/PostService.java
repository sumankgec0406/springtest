package com.css.voi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.css.voi.model.PostInfo;
import com.css.voi.model.Post_details;
import com.css.voi.repository.PostRepository;

@Service
public class PostService {
	
	@Autowired
	PostRepository postRepo;
	
	
	public PostInfo save(PostInfo posts) {
		return postRepo.save(posts);
	}
	
	
	public List<PostInfo> findAllPost(){
		return (List<PostInfo>) postRepo.findAll();
	}
	
	
	public Optional<PostInfo> findPostById(Long post_id) {
		return postRepo.findById(post_id);
	}
	
	public PostInfo updatePostInfo(PostInfo posts, Long post_id) {
		return postRepo.save(posts);
	}
	
	public void deletePostInfo(Long post_id) {
		postRepo.deleteById(post_id);
	}
}
