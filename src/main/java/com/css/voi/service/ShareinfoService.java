package com.css.voi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.css.voi.model.Shareinfo;
import com.css.voi.repository.ShareinfoRepository;
@Service
public class ShareinfoService {
 @Autowired
  private ShareinfoRepository shareinfoRepository;
 
	public List<Shareinfo> getAllShareinfo() {		
			List<Shareinfo> shares=new ArrayList<Shareinfo>();
			shareinfoRepository.findAll()
			 .forEach(shares::add);
			   return shares;
		// TODO Auto-generated method stub
		
	}
	

	public Optional<Shareinfo> getShareinfo(int id) {
		// TODO Auto-generated method stub
		return shareinfoRepository.findById(id);
	}

	public Shareinfo addShareinfo(Shareinfo shareinfo) {
		// TODO Auto-generated method stub
		return shareinfoRepository.save(shareinfo);
	}

	public Shareinfo updateShareinfo(Shareinfo shareinfo,int id) {
		// TODO Auto-generated method stub
		return shareinfoRepository.save(shareinfo);
	}

	public void deleteShareinfo(int id) {
		// TODO Auto-generated method stub
		shareinfoRepository.deleteById(id);
	}

}
