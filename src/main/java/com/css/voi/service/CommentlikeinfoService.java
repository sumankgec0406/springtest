package com.css.voi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.css.voi.model.Commentlikeinfo;
import com.css.voi.repository.CommentlikeinfoRepository;
@Service
public class CommentlikeinfoService {

	@Autowired 
	private CommentlikeinfoRepository  commentlikeinfoRepository;
	
	public List<Commentlikeinfo> getAllCommentlikeinfo() {
		// TODO Auto-generated method stub
		List<Commentlikeinfo> comments=new ArrayList<Commentlikeinfo>();
		commentlikeinfoRepository.findAll().forEach(comments::add);
		
		return comments;
	}

	public Optional<Commentlikeinfo> getCommentlikeinfo(int id) {
		// TODO Auto-generated method stub
		return commentlikeinfoRepository.findById(id);
	}

	public Commentlikeinfo addCommentlikeinfo(Commentlikeinfo commentlikeinfo) {
		// TODO Auto-generated method stub
		return commentlikeinfoRepository.save(commentlikeinfo);
		
		
	}

	public Commentlikeinfo updateCommentlikeinfo(Commentlikeinfo commentlikeinfo, int id) {
		// TODO Auto-generated method stub
		return commentlikeinfoRepository.save(commentlikeinfo);
		
		
	}

	public void deleteCommentlikeinfo(int id) {
		// TODO Auto-generated method stub
		commentlikeinfoRepository.deleteById(id);
		
	}

}
