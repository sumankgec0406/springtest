package com.css.voi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.css.voi.model.Post_details;
import com.css.voi.repository.PostDetailsRepository;



@Service
public class PostDetailsService {
	
	@Autowired
	PostDetailsRepository postdetailsRepo;
	
	/* to save post details*/
	public Post_details save(Post_details posts) {
		return postdetailsRepo.save(posts);
	}
	
	/* to get all post details */
	public List<Post_details> findAllPostDetails(){
		return (List<Post_details>) postdetailsRepo.findAll();
	}
	
	/* get post details by id */
	public Optional<Post_details> findPostDetailsById(Long post_detailsId) {
		return postdetailsRepo.findById(post_detailsId);
	}
	
	/* to update a post */
	public Post_details updatePostDetails(Post_details posts, Long post_detailsId) {
		return postdetailsRepo.save(posts);
	}
	
	public void deletePost(Long post_detailsId) {
		postdetailsRepo.deleteById(post_detailsId);
	}
}
