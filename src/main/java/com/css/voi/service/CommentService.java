package com.css.voi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.css.voi.model.Comment;
import com.css.voi.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	private CommentRepository commentReposotory;
	
	/* to save cooment information */
	public Comment saveComment(Comment comment) {
		return commentReposotory.save(comment);
	}
	
	/* to get all comment information */
	public List<Comment> getCommentdetails(){
		return (List<Comment>) commentReposotory.findAll();
	}
	
	public Optional<Comment> getCommentBiId(int commentid) {
		return commentReposotory.findById(commentid);
	}
	
	
	
	public void deletecomment(int commentid) {
		commentReposotory.deleteById(commentid);
	}

	public Comment updatecomment(Comment comment, int commentid) {
		// TODO Auto-generated method stub
		return commentReposotory.save(comment);

	}
}
