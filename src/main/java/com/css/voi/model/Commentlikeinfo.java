package com.css.voi.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @NoArgsConstructor
public class Commentlikeinfo {
	@Id
	private int id;
	private String uid;
	private int comment_commentid;
	public Commentlikeinfo(int id, String uid, int comment_commentid) {
		super();
		this.id = id;
		this.uid = uid;
		this.comment_commentid = comment_commentid;
	}
	

}
