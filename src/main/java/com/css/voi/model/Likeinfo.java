package com.css.voi.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @NoArgsConstructor
public class Likeinfo {
	@Id
	private int id;
	private String uid;
	private int post_postid;
	public Likeinfo(int id, String uid, int post_postid) {
		super();
		this.id = id;
		this.uid = uid;
		this.post_postid = post_postid;
	}
	

}
