package com.css.voi.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @NoArgsConstructor
public class Shareinfo {
	@Id
	private int idshare;
	private String uid;
	private String sharedon;
	private int post_postid;
	
	public Shareinfo(int idshare, String uid, String sharedon, int post_postid) {
		super();
		this.idshare = idshare;
		this.uid = uid;
		this.sharedon = sharedon;
		this.post_postid = post_postid;
	}

}
