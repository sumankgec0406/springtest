package com.css.voi.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * This is model class of posts
 * @author KAUSTABH-PC
 *
 */
@Entity
@Table(name="postinfo")
public class PostInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="post_id", unique=true)
	private Long post_id;
	
	@Column(name="postTitle")
	private String postTitle;
	
	@Column(name="postinfo")
	private String postinfo;
	
	@OneToOne(cascade=CascadeType.ALL)
	private Post_details post_details;
	
	public PostInfo() {
		
	}

	public PostInfo(Long post_id, String postTitle, String postinfo) {
		super();
		this.post_id = post_id;
		this.postTitle = postTitle;
		this.postinfo = postinfo;
	}

	public Long getPost_id() {
		return post_id;
	}

	public void setPost_id(Long post_id) {
		this.post_id = post_id;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostinfo() {
		return postinfo;
	}

	public void setPostinfo(String postinfo) {
		this.postinfo = postinfo;
	}

	public Post_details getPost_details() {
		return post_details;
	}

	public void setPost_details(Post_details post_details) {
		this.post_details = post_details;
	}
	
	
	
}
