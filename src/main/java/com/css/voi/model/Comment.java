package com.css.voi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="comment")
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="commentid", unique=true)
	private int commentid;
	
	@Column(name="comment")
	private String comment;
	
	@Column(name="username")
	private String username;
	
	@Column(name="post_postid")
	private int post_postid;
	
	@Column(name="likes")
	private int likes;
	
	public Comment() {
		
	}
	public Comment(int commentid, String comment, String username, int post_postid, int likes) {
		super();
		this.commentid = commentid;
		this.comment = comment;
		this.username = username;
		this.post_postid = post_postid;
		this.likes = likes;
	}
	public int getCommentid() {
		return commentid;
	}
	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getPost_postid() {
		return post_postid;
	}
	public void setPost_postid(int post_postid) {
		this.post_postid = post_postid;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	

}
