package com.css.voi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Past;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="post_details")
public class Post_details {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="post_detailsId", unique=true)
	private Long post_detailsId;
	
	@Column(name="noOfLikes")
	private String noOfLikes;
	
	@Column(name="noOfComments")
	private String noOfComments;
	
	@Column(name="createdOn")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	@Past
	private Date createdOn;
	
	@Column(name="deletedOn")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	@Past
	private Date deletedOn;
	
	@Column(name="updatedOn")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	@Past
	private Date updatedOn;
	
	@Column(name="noOfShares")
	private String noOfShares;
	
	@Column(name="subjects")
	private String subjects;
	
	
	public Post_details() {
		
	}
	
	
	public Post_details(Long post_detailsId, String noOfLikes, String noOfComments, @Past Date createdOn,
			@Past Date deletedOn, @Past Date updatedOn, String noOfShares, String subjects) {
		super();
		this.post_detailsId = post_detailsId;
		this.noOfLikes = noOfLikes;
		this.noOfComments = noOfComments;
		this.createdOn = createdOn;
		this.deletedOn = deletedOn;
		this.updatedOn = updatedOn;
		this.noOfShares = noOfShares;
		this.subjects = subjects;
	
	}


	public Long getPost_detailsId() {
		return post_detailsId;
	}



	public void setPost_detailsId(Long pots_detailsId) {
		this.post_detailsId = pots_detailsId;
	}



	public String getNoOfLikes() {
		return noOfLikes;
	}

	public void setNoOfLikes(String noOfLikes) {
		this.noOfLikes = noOfLikes;
	}

	public String getNoOfComments() {
		return noOfComments;
	}

	public void setNoOfComments(String noOfComments) {
		this.noOfComments = noOfComments;
	}

	

	public Date getCreatedOn() {
		return createdOn;
	}



	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	public Date getDeletedOn() {
		return deletedOn;
	}



	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}



	public Date getUpdatedOn() {
		return updatedOn;
	}



	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}



	public String getNoOfShares() {
		return noOfShares;
	}

	public void setNoOfShares(String noOfShares) {
		this.noOfShares = noOfShares;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}



	
	
	

}
