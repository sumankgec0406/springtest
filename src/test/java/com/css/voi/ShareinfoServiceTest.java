package com.css.voi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.css.voi.model.Shareinfo;
import com.css.voi.repository.ShareinfoRepository;
import com.css.voi.service.ShareinfoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShareinfoServiceTest {

	@Autowired
	private ShareinfoService service;
	@MockBean
	private ShareinfoRepository repository;

	@Test
	public void getAllShareinfoTest() {

		when(repository.findAll()).thenReturn(
				Stream.of(new Shareinfo(1, "user123", "share123", 3333), new Shareinfo(2, "user333", "share333", 4444))
						.collect(Collectors.toList()));
		assertEquals(2, service.getAllShareinfo().size());

	}

	@Test
	public void getShareinfoTest() {
		int id = 2;
		when(repository.findById(id)).thenReturn(Stream.of(new Shareinfo(2, "user333", "share333", 4444)).findAny());
		assertThat(service.getShareinfo(id)).isNotEmpty();
		System.out.println(service.getShareinfo(id).get());
	}

	@Test
	public void addShareinfoTest() {
		Shareinfo shareinfo = new Shareinfo(1, "user123", "share123", 3333);
		when(repository.save(shareinfo)).thenReturn(shareinfo);
		assertEquals(shareinfo, service.addShareinfo(shareinfo));
	}

	@Test
	public void deleteShareinfoTest() {
		Shareinfo shareinfo = new Shareinfo(1, "user123", "share123", 3333);
		when(repository.save(shareinfo)).thenReturn(shareinfo);
		service.deleteShareinfo(1);
		verify(repository, times(1)).deleteById(1);
	}
	
	public void updateShareinfoTest() {
		Shareinfo shareinfo = new Shareinfo(1, "user123", "share123", 3333);
		when(repository.save(shareinfo)).thenReturn(shareinfo);
		assertEquals(shareinfo, service.addShareinfo(shareinfo));
		
	}

}
